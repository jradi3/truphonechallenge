//
//  StationListTableViewController.swift
//  TruphoneChallenge
//
//  Created by Tarek Jradi on 15/03/2018.
//  Copyright © 2018 Thuphone. All rights reserved.
//

import UIKit

class StationListTableViewController: UITableViewController {

    /// The search bar
    let searchController = UISearchController(searchResultsController: nil)
    
    /// The station data parsed from the JSON station file
    let stationData = NSData(contentsOfFile: Bundle.main.path(forResource: "stations", ofType: "json")!)
    
    /// The stations as a JSON object
    var stations: JSON?
    
    /// The stations filtered based on what is typed in the search bar
    var filteredStations = [JSON]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /// Set the navigation graphic primitives to a light blue color
        let lightBlue = UIColor(red: 0.0/255.0, green: 162.0/255.0, blue: 227.0/255.0, alpha: 1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: lightBlue]
        
        /// Do the actual JSON parsing
        do {
            stations = try JSON(data: stationData! as Data)
        } catch {
            // Do nothing for now
        }
        
        /// Search bar interaction - make it visible from the get-go
        searchController.searchResultsUpdater = self
        searchController.dimsBackgroundDuringPresentation = false
        definesPresentationContext = true
        tableView.tableHeaderView = searchController.searchBar
    }
    
    func filterContentForSearch(searchText: String) {
        filteredStations = stations!.array!.filter { station in
            return station["name"].stringValue.lowercased().contains(searchText.lowercased())
        }
        
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            return filteredStations.count
        }
        
        return stations!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "stationCell", for: indexPath)
        
        var data: JSON
        
        if searchController.isActive && searchController.searchBar.text != "" {
            data = filteredStations[indexPath.row]
        }  else {
            data = stations![indexPath.row]
        }
        
        let stationName = data["name"].stringValue
        let stationSymbol = data["symbol"].stringValue
        
        cell.textLabel?.text = stationName
        cell.detailTextLabel?.text = stationSymbol
        
        return cell
    }
        
    @IBAction func backBarButtonItemTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension StationListTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearch(searchText: searchController.searchBar.text!)
    }
}
